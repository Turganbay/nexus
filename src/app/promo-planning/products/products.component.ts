import { Component, OnInit } from '@angular/core';
import { CRUDTableConfig } from 'src/app/core/models/common';
import { productCategoryTableConfig } from 'src/app/core/models';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  tableConfig: CRUDTableConfig = productCategoryTableConfig;
  data = [
    { Id: 1, Name: 'Category 1', Code: '001' },
    { id: 2, Name: 'Category 2', Code: '002' }
  ];

  constructor() { }

  ngOnInit() {
  }

}
