import { Component, OnInit } from '@angular/core';
import { DictionaryService } from 'src/app/core/services/dictinary.service';
import { forkJoin } from 'rxjs';
import { PromoAddComponent } from '../promo-add/promo-add.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-promo-planning-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {
  lines: any[];
  periods: any[];
  activities: any[];

  data: any[][] = [];

  pageSize = 0;
  count = 4;

  currentPeriod: any[];

  constructor(
    private dictionaryService: DictionaryService,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    this.getData();

    this.dictionaryService.changePage.subscribe((bool: boolean) => {
      this.nextPrevPage(bool);
    });

    this.dictionaryService.disabledPrevBtn.next(true);

    this.dictionaryService.dialogPageOnSaveEvent.subscribe(res => {
      this.getData();
    });
  }

  getData = () => {

    const lines = this.dictionaryService.getDictinary('ProductLines');
    const periods = this.dictionaryService.getDictinary('MarketingPeriods');
    const activities = this.dictionaryService.getDictinary('MarketingActivities');


    forkJoin(lines, periods, activities).subscribe(res => {
      this.lines = res[0].value;
      this.periods = res[1].value;
      this.activities = res[2].value;

      this.currentPeriod = this.periods.slice(0, 4);

      this.generate();

    });
  }

  generate = () => {
    this.lines.forEach(line => {
      this.data[line.Id] = [];
      this.periods.forEach(period => {
        const act: any = this.activities.find(i => i.ProductLineId === line.Id && i.MarketingPeriodId === period.Id);
        if (act) {
          this.data[line.Id][period.Id] = act;
        }
      });
    });

    console.log('generate', this.data);

  }

  onClick = (act: any) => {
    const dialogRef = this.dialog.open(PromoAddComponent, {
      height: '734px',
      width: '711px',
      data: {
        periodId: act.MarketingPeriodId,
        productLineId: act.ProductLineId
      }
    });

    dialogRef.afterClosed().subscribe(result => {
    });

  }

  nextPrevPage = (bool: boolean) => {

    console.log('next prev page', bool);
    if (bool && this.pageSize * 4 < this.periods.length) {
      this.pageSize++;
    } else if (!bool && this.pageSize > 0) {
      this.pageSize--;
    }

    this.dictionaryService.disabledPrevBtn.next(this.pageSize === 0 ? true : false);
    this.dictionaryService.disabledNextBtn.next(this.pageSize * 4 + 4 < this.periods.length ? false : true);

    this.currentPeriod = this.periods.slice(this.pageSize * 4,  this.pageSize * 4 + 4);
  }


}
