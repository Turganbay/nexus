import { Component, OnInit, Input } from '@angular/core';
import { IMarketingActivity } from '../promo-add/promo-add.component';
import { Observable } from 'rxjs';
import { DictionaryService } from 'src/app/core/services/dictinary.service';

@Component({
  selector: 'app-main-info',
  templateUrl: './main-info.component.html',
  styleUrls: ['./main-info.component.scss']
})
export class MainInfoComponent implements OnInit {

  clients: Observable<any>;
  marketingActivityTypes: Observable<any>;
  marketingPeriods: Observable<any>;
  productCategories: Observable<any>;
  productBrands: Observable<any>;
  productLines: Observable<any>;
  activityStatus: Observable<any>;

  @Input() mainInfo: IMarketingActivity;

  constructor(
    private dictionaryService: DictionaryService
  ) { }

  ngOnInit() {
    this.clients = this.dictionaryService.getDictinary('Clients');
    this.marketingActivityTypes = this.dictionaryService.getDictinary('MarketingActivityTypes');
    this.marketingPeriods = this.dictionaryService.getDictinary('MarketingPeriods');
    this.productCategories = this.dictionaryService.getDictinary('ProductCategories');
    this.productBrands = this.dictionaryService.getDictinary('ProductBrands');
    this.productLines = this.dictionaryService.getDictinary('ProductLines');
    this.activityStatus = this.dictionaryService.getDictinary('ActivityStatus');
  }

}
