import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromoPlanningOutletComponent } from './promo-planning-outlet.component';

describe('PromoPlanningOutletComponent', () => {
  let component: PromoPlanningOutletComponent;
  let fixture: ComponentFixture<PromoPlanningOutletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromoPlanningOutletComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromoPlanningOutletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
