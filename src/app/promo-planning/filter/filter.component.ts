import { Component, OnInit } from '@angular/core';
import { DictionaryService } from 'src/app/core/services/dictinary.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-promo-planning-filter',
  templateUrl: './filter.component.html'
})
export class FilterComponent implements OnInit {
  clients: Observable<any>;
  categories: Observable<any>;
  brands: Observable<any>;

  disablePrevBtn = false;
  disableNextBtn = false;

  constructor(
    private dictionaryService: DictionaryService
  ) { }

  ngOnInit() {
    this.clients = this.dictionaryService.getDictinary('Clients');
    this.categories = this.dictionaryService.getDictinary('ProductCategories');
    this.brands = this.dictionaryService.getDictinary('ProductBrands');


    this.dictionaryService.disabledPrevBtn.subscribe((btn: boolean) => {
      this.disablePrevBtn = btn;
    });

    this.dictionaryService.disabledNextBtn.subscribe((btn: boolean) => {
      this.disableNextBtn = btn;
    });

  }

  nextPrevPage(bool: boolean) {
    this.dictionaryService.changePage.next(bool);
  }

}
