import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogTitles } from 'src/app/constants';
import { DictionaryService } from 'src/app/core/services/dictinary.service';

export interface IMarketingActivity {
  Id?: number;
  MarketingPeriodId?: number;
  BudgetTypeId?: number;
  ClientId?: number;
  MarketingActivityTypeId?: number;
  ActivityStatusId?: number;
  Name?: number;
  ProductCategoryId?: number;
  ProductBrandId?: number;
  ProductLineId?: number;
  Discount?: number;
  MarginPercent?: number;
  PlanedSales?: number;
  PlanedSalesAmount?: number;
}


@Component({
  selector: 'app-promo-add',
  templateUrl: './promo-add.component.html',
  styleUrls: ['./promo-add.component.scss']
})
export class PromoAddComponent implements OnInit {
  dialogTitle = DialogTitles.ADD_PROMO;
  page = 'mainInfo';
  marketingActivityModel: IMarketingActivity;
  DICTIONARY_NAME = 'MarketingActivities';
  menu = [
    {
      page: 'mainInfo',
      title: 'Основное инфо'
    },
    {
      page: 'products',
      title: 'Продукты'
    },
    {
      page: 'shops',
      title: 'Список магазинов'
    },
    {
      page: 'display',
      title: 'Дисплей по точкам'
    },
  ];

  constructor(
    public dialogRef: MatDialogRef<PromoAddComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dictionyService: DictionaryService
  ) { }

  ngOnInit() {
    this.marketingActivityModel = { Id: 0, MarketingPeriodId: this.data.periodId };
    this.dictionyService.getDictinary(`MarketingActivities?$filter=MarketingPeriodId eq ${this.data.periodId}
        and ProductLineId eq ${this.data.productLineId}`).subscribe(res => {
      if (res.value.length > 0) {
        this.marketingActivityModel = res.value[0];
      }
    });
  }

  changePage = (item: any) => {
    this.page = item.page;
  }

  close = () => {
    this.dialogRef.close();
  }

  save = () => {
    if (this.marketingActivityModel.Id === 0) {
      this.dictionyService.postDictionary(this.DICTIONARY_NAME, this.marketingActivityModel).subscribe(res => {
        this.close();
        this.dictionyService.dialogPageOnSaveEvent.next();
      });
    } else {
      this.dictionyService.patchDictionary(this.DICTIONARY_NAME, this.marketingActivityModel).subscribe(res => {
        this.close();
        this.dictionyService.dialogPageOnSaveEvent.next();
      });
    }
  }
}
