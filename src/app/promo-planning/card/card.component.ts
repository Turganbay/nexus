import { Component, OnInit, Input } from '@angular/core';
import { CARD_STATUS } from 'src/app/constants';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  @Input() status = CARD_STATUS.TIME;
  @Input() width = 1 ;
  @Input() value = 0;

  constructor() { }

  ngOnInit() {
  }

}
