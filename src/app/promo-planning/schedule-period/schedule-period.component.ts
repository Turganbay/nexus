import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import {  } from '@angular/core/testing';
import { DialogTitles } from 'src/app/constants';
import { CRUDTableConfig } from 'src/app/core/models/common';
import { DictionaryService } from 'src/app/core/services/dictinary.service';
import { schedulePeriodTableConfig } from 'src/app/core/models';
import { PromoAddComponent } from '../promo-add/promo-add.component';

export interface IDatePicker {
  DateStart?: string;
  DateFinish?: string;
}

@Component({
  selector: 'app-schedule-period',
  templateUrl: './schedule-period.component.html',
  styleUrls: ['./schedule-period.component.scss']
})
export class SchedulePeriodComponent implements OnInit {
  dialogTitle = DialogTitles.PERIOD;
  tableConfig: CRUDTableConfig;
  datePickerModel: IDatePicker = {};
  DICTIONARY_NAME = 'MarketingPeriods';

  data: any;
  constructor(
    public dialogRef: MatDialogRef<SchedulePeriodComponent>,
    @Inject(MAT_DIALOG_DATA) public data1: any,
    private dictionaryService: DictionaryService,
    public dialog: MatDialog
  ) { }

  ngOnInit() {
    console.log('tableconfig', this.tableConfig);
    this.tableConfig = schedulePeriodTableConfig;
    this.getData();
  }

  getData = () => {
    this.dictionaryService.getDictinary(this.DICTIONARY_NAME).subscribe(res => {
      console.log('res', res);
      this.data = res.value;
    });
  }

  close = () => {
    this.dialogRef.close();
  }

  add = () => {
    console.log('add', this.datePickerModel);
    this.dictionaryService.postDictionary(this.DICTIONARY_NAME, this.datePickerModel).subscribe(res => {
      console.log('res', res);
      this.getData();
    });
  }

  edit = (id: number) => {
    console.log('edit', id);
    this.close();

    const dialogRef = this.dialog.open(PromoAddComponent, {
      height: '734px',
      width: '711px',
      data: { periodId: id }
    });

  }

  save = () => {
    this.close();
  }
}
