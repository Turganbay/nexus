import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PromoPlanningOutletComponent } from './promo-planning-outlet/promo-planning-outlet.component';

const routes: Routes = [
  {
    path: '',
    component: PromoPlanningOutletComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PromoPlanningRoutingModule { }
