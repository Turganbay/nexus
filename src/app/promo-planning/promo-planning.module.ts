import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PromoPlanningRoutingModule } from './promo-planning-routing.module';
import { PromoPlanningOutletComponent } from './promo-planning-outlet/promo-planning-outlet.component';

import { SharedModule } from '../shared/shared.module';
import { HeaderComponent } from './header/header.component';
import { FilterComponent } from './filter/filter.component';
import { TableComponent } from './table/table.component';
import { CardComponent } from './card/card.component';
import { SchedulePeriodComponent } from './schedule-period/schedule-period.component';
import { PromoAddComponent } from './promo-add/promo-add.component';
import { MainInfoComponent } from './main-info/main-info.component';
import { ProductsComponent } from './products/products.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatNativeDateModule } from '@angular/material/core';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    PromoPlanningOutletComponent,
    HeaderComponent,
    FilterComponent,
    TableComponent,
    CardComponent,
    SchedulePeriodComponent,
    PromoAddComponent,
    MainInfoComponent,
    ProductsComponent
  ],
  imports: [
    CommonModule,
    PromoPlanningRoutingModule,
    SharedModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    FormsModule
  ],
  entryComponents: [
    SchedulePeriodComponent,
    PromoAddComponent,
  ],
  providers: [
    MatDatepickerModule
  ]
})
export class PromoPlanningModule { }
