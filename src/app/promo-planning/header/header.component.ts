import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { SchedulePeriodComponent } from '../schedule-period/schedule-period.component';
import { PromoAddComponent } from '../promo-add/promo-add.component';

@Component({
  selector: 'app-promo-planning-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(
    public dialog: MatDialog
  ) { }

  ngOnInit() { }

  openDialog = (type: string) => {
    console.log('open dialog');
    const dialogComponent: any = type === 'period' ? SchedulePeriodComponent : PromoAddComponent;

    const dialogRef = this.dialog.open(dialogComponent, {
      height: '734px',
      width: '711px',
      data: { periodId: 0 }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.animal = result;
    });

  }

}
