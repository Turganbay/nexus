import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportsOutletComponent } from './reports-outlet.component';

describe('ReportsOutletComponent', () => {
  let component: ReportsOutletComponent;
  let fixture: ComponentFixture<ReportsOutletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportsOutletComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportsOutletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
