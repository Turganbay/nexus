import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReportsOutletComponent } from './reports-outlet/reports-outlet.component';

const routes: Routes = [
  {
    path: '',
    component: ReportsOutletComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportsRoutingModule { }
