export enum CARD_STATUS {
    TIME  = 0,
    DONE = 1,
    EDIT = 2,
    CANCEL = 3
}

export enum DialogTitles {
    PERIOD = 'Залпанировать период',
    ADD_PROMO = 'Активность'
}