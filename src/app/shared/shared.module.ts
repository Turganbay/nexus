import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { RouterModule } from '@angular/router';
import { MatDialogModule } from '@angular/material/dialog';
// import { MatDatepickerModule } from '@angular/material/datepicker';
import { CrudTableComponent } from './crud-table/crud-table.component';

import { FormsModule } from '@angular/forms';
import { DialogFooterComponent } from './dialog/footer/footer.component';
import { DialogHeaderComponent } from './dialog/header/header.component';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from '../core/core.module';

import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatNativeDateModule } from '@angular/material/core';

@NgModule({
  declarations: [
    HeaderComponent,
    CrudTableComponent,
    DialogFooterComponent,
    DialogHeaderComponent
  ],
  imports: [
    CoreModule,
    CommonModule,
    RouterModule,
    MatDialogModule,
    FormsModule,
    HttpClientModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule
  ],
  exports: [
    HeaderComponent,
    CrudTableComponent,
    DialogFooterComponent,
    DialogHeaderComponent
  ]
})
export class SharedModule { }
