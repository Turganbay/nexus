import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DictionaryService } from 'src/app/core/services/dictinary.service';

@Component({
  selector: 'app-dialog-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class DialogFooterComponent implements OnInit {

// tslint:disable-next-line: no-output-on-prefix
  @Output() onClosed = new EventEmitter();
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onSave = new EventEmitter();

  constructor(
    private dictionaryService: DictionaryService
  ) { }

  ngOnInit() {
  }

  close = () => {
    this.onClosed.emit();
  }

  save = () => {
    console.log('save');
    this.onSave.emit();
    this.dictionaryService.dialogPageOnSaveEvent.next();
  }
}
