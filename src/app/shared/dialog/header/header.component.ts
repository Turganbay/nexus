import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-dialog-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class DialogHeaderComponent implements OnInit {

  @Input() title = 'Title';
// tslint:disable-next-line: no-output-on-prefix
  @Output() onClosed = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  close = () => {
    this.onClosed.emit();
  }

}
