import { Component, OnInit } from '@angular/core';

export interface IMenu {
  link: string;
  title: string;
}

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  menu: IMenu[] = [
    {
      link: '/promo-planning',
      title: 'Планирование промо'
    },
    {
      link: './sfd',
      title: 'Планирование продаж'
    },
    {
      link: './dfd',
      title: 'Правила'
    },
    {
      link: '/reports',
      title: 'Отчеты'
    },
    {
      link: '/directory',
      title: 'Справочники'
    }
  ];

  constructor() { }

  ngOnInit() { }

}
