import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CRUDTableConfig } from 'src/app/core/models/common';
import { DictionaryService } from 'src/app/core/services/dictinary.service';

export interface IActivityStatus {
  id: number;
  value: string;
  edit: boolean;
}

@Component({
  selector: 'app-crud-table',
  templateUrl: './crud-table.component.html',
  styleUrls: ['./crud-table.component.scss']
})
export class CrudTableComponent implements OnInit {
  @Input() tableConfig: CRUDTableConfig;
  @Input() data = [];

  // tslint:disable-next-line: no-output-on-prefix
  @Output() onSave = new EventEmitter();
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onCancel = new EventEmitter<any>();
  // tslint:disable-next-line: no-output-on-prefix
  @Output() onEdit = new EventEmitter<number>();

  constructor(
    private dictionaryService: DictionaryService
  ) { }

  ngOnInit() {
    console.log('tab c', this.tableConfig);
  }

  edit = (item: any) => {
    item.edit = true;

    // if (this.tableConfig.name === 'SchedulteStatus') {
    //   this.onEdit.emit(item.Id);
    // } else {
    //   item.edit = true;
    // }
  }

  delete = (item: any) => {
  }

  cancel = (item: any) => {
    item.edit = false;
    if (item.Id === 0) {
      this.onCancel.emit(item);
    }
  }

  save = (item: any) => {
    item.edit = false;
    delete item.edit;
    delete item.Store;
    delete item.Client;

    if (item.Id !== 0) {
      this.dictionaryService.patchDictionary(this.tableConfig.url.match(/\w+/gi)[0], item).subscribe(res => {
        this.onSave.emit();
      });
    } else {
      this.dictionaryService.postDictionary(this.tableConfig.url.match(/\w+/gi)[0], item).subscribe(res => {
        console.log('res', res);
        this.onSave.emit();
      })
    }

  }

  ngOnChange = (obj) => {
    console.log('onchange', obj);
  }
}
