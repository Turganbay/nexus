import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DirectoryRoutingModule } from './directory-routing.module';
import { DirectoryOutletComponent } from './directory-outlet/directory-outlet.component';
import { SharedModule } from '../shared/shared.module';
import { ActivityStatusComponent } from './activity-status/activity-status.component';
import { CoreModule } from '../core/core.module';


@NgModule({
  declarations: [DirectoryOutletComponent, ActivityStatusComponent],
  imports: [
    CommonModule,
    DirectoryRoutingModule,
    SharedModule,
    CoreModule
    // FormsModule
  ]
})
export class DirectoryModule { }
