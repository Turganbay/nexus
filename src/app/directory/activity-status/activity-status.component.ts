import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CRUDTableConfig } from 'src/app/core/models/common';
import { DictionaryService } from 'src/app/core/services/dictinary.service';
import {
  budgetTypesTableConfig,
  activityStatusTableConfig,
  activityTypesTableConfig,
  marketingActivityTypesTableConfig,
  clientTableConfig,
  productBrandTableConfig,
  productCategoryTableConfig,
  storePlaceTableConfig,
  productLinkTableConfig,
  productLineTableConfig,
  productStockKeepingUnitTableConfig,
  storeTableConfig
} from 'src/app/core/models';

export interface IActivityStatus {
  id: number;
  value: string;
  edit: boolean;
}

@Component({
  selector: 'app-activity-status',
  templateUrl: './activity-status.component.html'
})
export class ActivityStatusComponent implements OnInit {
  tableConfig: CRUDTableConfig;
  data = [];
  type: string;

  constructor(
    private route: ActivatedRoute,
    private dictionaryService: DictionaryService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.type = params.name;
      this.getTableConfig();
      this.getData();
    });
  }

  getData = () => {
    this.dictionaryService.getDictinary(this.tableConfig.url).subscribe(res => {
      this.data = res.value;
    });
  }

  checkSelect = () => {
    console.log('check select');
    this.tableConfig.fields.forEach(i => {
      if (i.type === 'select') {
        this.dictionaryService.getDictinary(i.rel).subscribe(res => {
          i.list = res.value;
          console.log(this.tableConfig);
        });
      }
    });
  }

  update = () => {
    this.getData();
  }

  cancel = (item: any) => {
    const index = this.data.findIndex(i => i.Id === item.Id);
    this.data.splice(index, 1);
  }

  add = () => {
    if (!this.data.find(i => i.Id === 0)) {
      const item = { edit: true};
      this.tableConfig.fields.map(i => i.key).forEach(k => {
        item[k] = k === 'Id' ? 0 : '';
      });
      this.data.unshift(item);
      console.log('item', item);
    }
  }

  getTableConfig = () => {
    switch (this.type) {
      case 'activity-status': {
        this.tableConfig = activityStatusTableConfig;
        break;
      }
      case 'activity-types': {
        this.tableConfig = activityTypesTableConfig;
        break;
      }
      case 'marketing-activity-types': {
        this.tableConfig = marketingActivityTypesTableConfig;
        break;
      }
      case 'budget-types': {
        this.tableConfig = budgetTypesTableConfig;
        break;
      }
      case 'clients': {
        this.tableConfig = clientTableConfig;
        break;
      }
      case 'product-brands': {
        this.tableConfig = productBrandTableConfig;
        break;
      }
      case 'product-categories': {
        this.tableConfig = productCategoryTableConfig;
        break;
      }
      case 'product-lines': {
        this.tableConfig = productLineTableConfig;
        break;
      }
      case 'product-stock-keeping-units': {
        this.tableConfig = productStockKeepingUnitTableConfig;
        break;
      }
      case 'stores': {
        this.tableConfig = storeTableConfig;
        break;
      }
      case 'store-places': {
        this.tableConfig = storePlaceTableConfig;
        break;
      }
      case 'product-links': {
        this.tableConfig = productLinkTableConfig;
        break;
      }
    }

    this.checkSelect();
  }

}
