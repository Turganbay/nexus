import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DirectoryOutletComponent } from './directory-outlet/directory-outlet.component';
import { ActivityStatusComponent } from './activity-status/activity-status.component';

const routes: Routes = [
  {
    path: '',
    component: DirectoryOutletComponent,
    children: [
      {
        path: ':name',
        component: ActivityStatusComponent
      },
      {
        path: '',
        redirectTo: 'activity-status'
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DirectoryRoutingModule { }
