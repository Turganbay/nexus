import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-directory-outlet',
  templateUrl: './directory-outlet.component.html',
  styleUrls: ['./directory-outlet.component.scss']
})
export class DirectoryOutletComponent implements OnInit {

  navigation = [
    {
      link: './activity-status',
      title: 'Active status'
    },
    {
      link: './activity-types',
      title: 'Activity Types'
    },
    {
      link: './marketing-activity-types',
      title: 'Marketing Activity Types'
    },
    {
      link: './budget-types',
      title: 'Budget Types'
    },
    {
      link: './clients',
      title: 'Client'
    },
    {
      link: './product-brands',
      title: 'Product Brand'
    },
    {
      link: './product-categories',
      title: 'Product Category'
    },
    {
      link: './product-lines',
      title: 'Product Line'
    },
    {
      link: './product-links',
      title: 'Product Link'
    },
    {
      link: './product-stock-keeping-units',
      title: 'Product Stock Keeping Unit'
    },
    {
      link: './stores',
      title: 'Store'
    },
    {
      link: './store-places',
      title: ' Store Place'
    },
  ];

  constructor() { }

  ngOnInit() {
  }

}
