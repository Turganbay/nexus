import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DirectoryOutletComponent } from './directory-outlet.component';

describe('DirectoryOutletComponent', () => {
  let component: DirectoryOutletComponent;
  let fixture: ComponentFixture<DirectoryOutletComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DirectoryOutletComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DirectoryOutletComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
