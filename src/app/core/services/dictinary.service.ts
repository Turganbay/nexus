import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class DictionaryService {

    // BASE_URL = 'http://192.168.1.180:7200/odata/';
    BASE_URL = 'http://89.219.13.204:7200/odata/';

    dialogPageOnSaveEvent = new Subject();
    changePage = new Subject<boolean>();
    disabledPrevBtn = new Subject<boolean>();
    disabledNextBtn = new Subject<boolean>();

    constructor(private http: HttpClient) {

    }

    getDictinary(type: any): Observable<any> {
        return this.http.get(`${this.BASE_URL}${type}`);
    }

    patchDictionary(type: any, item: any): Observable<any> {
        return this.http.patch(`${this.BASE_URL}${type}(${item.Id})`, item);
    }

    postDictionary(type: any, item: any): Observable<any> {
        return this.http.post(`${this.BASE_URL}${type}`, item);
    }
}