export interface List {
    Id: number;
    Name: string;
}

export interface CRUDTableConfig {
    name: string;
    fields: Field[];
    addLabel: string;
    url?: string;
}

export interface Field {
    name: string;
    key: string;
    type?: 'string' | 'number' | 'select' | 'date';
    isEditable?: boolean;
    list?: List[];
    expand?: string;
    rel?: string;
}