import { CRUDTableConfig } from './common';

export const budgetTypesTableConfig: CRUDTableConfig = {
    name: 'Budget Types',
    addLabel: 'Добавить budget types',
    url: 'BudgetTypes',
    fields: [
        {
            name: 'ID',
            key: 'Id',
        },
        {
            name: 'NAME',
            key: 'Name',
            type: 'string',
            isEditable: true,
        }
    ],
}