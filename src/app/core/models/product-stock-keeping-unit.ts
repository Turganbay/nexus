import { CRUDTableConfig } from './common';

export const productStockKeepingUnitTableConfig: CRUDTableConfig = {
    name: 'Product Stock Keeping Unit',
    addLabel: 'Добавить Product Stock Keeping Unit',
    url: 'ProductStockKeepingUnits',
    fields: [
        {
            name: 'ID',
            key: 'Id',
        },
        {
            name: 'NAME',
            key: 'Name',
            type: 'string',
            isEditable: true,
        },
        {
            name: 'CODE',
            key: 'Code',
            type: 'string',
            isEditable: true,
        },
    ],
};