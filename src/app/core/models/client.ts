import { CRUDTableConfig } from './common';

export const clientTableConfig: CRUDTableConfig = {
    name: 'Clients',
    addLabel: 'Добавить CLIENT',
    url: 'Clients',
    fields: [
        {
            name: 'ID',
            key: 'Id',
        },
        {
            name: 'NAME',
            key: 'Name',
            type: 'string',
            isEditable: true,
        }
    ],
}