import { CRUDTableConfig } from './common';

export const storeTableConfig: CRUDTableConfig = {
    name: 'Stores',
    addLabel: 'Добавить Store',
    url: 'Stores?$expand=Client',
    fields: [
        {
            name: 'ID',
            key: 'Id',
        },
        {
            name: 'NAME',
            key: 'Name',
            type: 'string',
            isEditable: true,
        },
        {
            name: 'CLIENT ID',
            key: 'ClientId',
            type: 'select',
            isEditable: true,
            rel: 'Clients',
            expand: 'Client',
            list: []
        },
    ],
}