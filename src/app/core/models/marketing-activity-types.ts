import { CRUDTableConfig } from './common';

export const marketingActivityTypesTableConfig: CRUDTableConfig = {
    name: 'Markting Activity Types',
    addLabel: 'Добавить Marketing Activity Types',
    url: 'MarketingActivityTypes',
    fields: [
        {
            name: 'ID',
            key: 'Id',
        },
        {
            name: 'NAME',
            key: 'Name',
            type: 'string',
            isEditable: true,
        }
    ],
}