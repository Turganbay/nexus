import { CRUDTableConfig } from './common';

export const activityStatusTableConfig: CRUDTableConfig = {
    name: 'Activity Status',
    url: 'ActivityStatus',
    addLabel: 'Добавить статус',
    fields: [
        {
            name: 'ID',
            key: 'Id',
        },
        {
            name: 'NAME',
            key: 'Name',
            type: 'string',
            isEditable: true,
        }
    ],
};