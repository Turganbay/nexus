import { CRUDTableConfig } from './common';

export const schedulePeriodTableConfig: CRUDTableConfig = {
    name: 'ScheduleStatus',
    addLabel: 'Добавить период',
    url: 'MarketingPeriods',
    fields: [
        {
            name: 'ID',
            key: 'Id',
        },
        {
            name: 'Date From',
            key: 'DateStart',
            type: 'date',
            isEditable: true,
        },
        {
            name: 'Date To',
            key: 'DateFinish',
            type: 'date',
            isEditable: true,
        }
    ],
}