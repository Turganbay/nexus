import { CRUDTableConfig } from './common';

export const productBrandTableConfig: CRUDTableConfig = {
    name: 'Product Brands',
    addLabel: 'Добавить product brand',
    url: 'ProductBrands',
    fields: [
        {
            name: 'ID',
            key: 'Id',
        },
        {
            name: 'NAME',
            key: 'Name',
            type: 'string',
            isEditable: true,
        },
        {
            name: 'CODE',
            key: 'Code',
            type: 'string',
            isEditable: true,
        }
    ],
}