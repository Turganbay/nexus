import { CRUDTableConfig } from './common';

export const productCategoryTableConfig: CRUDTableConfig = {
    name: 'Product Categories',
    addLabel: 'Добавить product category',
    url: 'ProductCategories',
    fields: [
        {
            name: 'ID',
            key: 'Id',
        },
        {
            name: 'NAME',
            key: 'Name',
            type: 'string',
            isEditable: true,
        },
        {
            name: 'CODE',
            key: 'Code',
            type: 'string',
            isEditable: true,
        },
    ],
};