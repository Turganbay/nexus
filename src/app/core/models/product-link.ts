import { CRUDTableConfig } from './common';

export const productLinkTableConfig: CRUDTableConfig = {
    name: 'Product Link',
    addLabel: 'Добавить Product Link',
    url: 'ProductLinks?$expand=ProductCategory,ProductBrand,ProductLine,ProductStockKeepingUnit',
    fields: [
        {
            name: 'ID',
            key: 'Id',
        },
        {
            name: 'PRODUCT CATEGORY ID',
            key: 'ProductCategoryId',
            type: 'select',
            isEditable: true,
            rel: 'ProductCategories',
            expand: 'ProductCategory',
            list: []
        },
        {
            name: 'PRODUCT BRAND ID',
            key: 'ProductBrandId',
            type: 'select',
            isEditable: true,
            rel: 'ProductBrands',
            expand: 'ProductBrand',
            list: []
        },
        {
            name: 'PRODUCT LINE ID',
            key: 'ProductLineId',
            type: 'select',
            isEditable: true,
            rel: 'ProductLines',
            expand: 'ProductLine',
            list: []
        },
        {
            name: 'PRODUCT STOCK KEEPING UNIT ID',
            key: 'ProductStockKeepingUnitId',
            type: 'select',
            isEditable: true,
            rel: 'ProductStockKeepingUnits',
            expand: 'ProductStockKeepingUnit',
            list: []
        },
        {
            name: 'PRICE FOR ITEM',
            key: 'PriceForItem',
            type: 'string',
            isEditable: true
        },
    ],
}