import { CRUDTableConfig } from './common';

export const activityTypesTableConfig: CRUDTableConfig = {
    name: 'Activity Types',
    addLabel: 'Добавить тип',
    url: 'ActivityTypes',
    fields: [
        {
            name: 'ID',
            key: 'Id',
        },
        {
            name: 'NAME',
            key: 'Name',
            type: 'string',
            isEditable: true,
        }
    ],
}