import { CRUDTableConfig } from './common';

export const productLineTableConfig: CRUDTableConfig = {
    name: 'Product Lines',
    addLabel: 'Добавить product line',
    url: 'ProductLines',
    fields: [
        {
            name: 'ID',
            key: 'Id',
        },
        {
            name: 'NAME',
            key: 'Name',
            type: 'string',
            isEditable: true,
        },
        {
            name: 'CODE',
            key: 'Code',
            type: 'string',
            isEditable: true,
        },
    ],
};