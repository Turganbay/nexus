import { CRUDTableConfig } from './common';

export const storePlaceTableConfig: CRUDTableConfig = {
    name: 'Store Place',
    addLabel: 'Добавить Store Place',
    url: 'StorePlaces?$expand=Store',
    fields: [
        {
            name: 'ID',
            key: 'Id',
        },
        {
            name: 'NAME',
            key: 'Name',
            type: 'string',
            isEditable: true,
        },
        {
            name: 'STORE ID',
            key: 'StoreId',
            type: 'select',
            isEditable: true,
            rel: 'Stores',
            expand: 'Store',
            list: []
        },
    ],
}